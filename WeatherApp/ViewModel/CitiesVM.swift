//
//  CitiesVM.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 24.11.2020.
//

import Foundation
import RxCocoa

final class CitiesVM {
    let dataSource = BehaviorRelay<[CityWeather]?>(value: nil)
}
