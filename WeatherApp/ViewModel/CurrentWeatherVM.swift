//
//  CurrentWeatherVM.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 16.11.2020.
//

import Foundation
import RxCocoa

final class CurrentWeatherVM {
    let weather = BehaviorRelay<ViewCityWeather?>(value: nil)
}
