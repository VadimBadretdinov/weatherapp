//
//  MainVM.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 18.11.2020.
//

import Foundation
import RxCocoa
import RxSwift

final class MainVM {

    private let disposeBag = DisposeBag()
    private let networkManager = NetworkManager()
    private let dbManager = DBManager()

    let currentWeather = BehaviorRelay<ViewCityWeather?>(value: nil)
    let forecastWeather = BehaviorRelay<ViewCityWeatherForecast?>(value: nil)

    func obtainWeather() {
        if let weather = dbManager.fetchCurrentWeather() {
            currentWeather.accept(weather)
        } else {
            networkManager.downloadWeather { (result) in
                switch result {
                case .failure:
                    self.currentWeather.accept(nil)
                case .success(let value):
                    let weather = ViewCityWeather(networkWeather: value)
                    self.dbManager.saveCurrentWeather(weather: weather)
                    self.currentWeather.accept(weather)
                }
            }
        }

        if let weather = dbManager.fetchForecastWeather() {
            forecastWeather.accept(weather)
        } else {
            networkManager.downloadForecastWeather(completion: { result in
                switch result {
                case .failure:
                    self.forecastWeather.accept(nil)
                case .success(let value):
                    self.forecastWeather.accept(ViewCityWeatherForecast(weather: value))
                }
            })
        }
    }
}
