//
//  ForecastWeatherDetailVM.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 24.11.2020.
//

import Foundation
import RxCocoa

final class ForecastWeatherDetailVM {
    let weather = BehaviorRelay<ViewCityWeatherForecast?>(value: nil)
    let currentWeather = BehaviorRelay<ViewForecastCityWeather?>(value: nil)
}
