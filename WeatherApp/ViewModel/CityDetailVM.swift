//
//  CityDetailVM.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 08.11.2020.
//

import UIKit
import SwiftDate
import RxCocoa

final class CityDetailVM {

    let currentWeather = BehaviorRelay<ViewCityWeather?>(value: nil)
    let forecastWeather  = BehaviorRelay<ViewCityWeatherForecast?>(value: nil)

    func forecastDaysCount(weather: ViewCityWeatherForecast) -> Int {
        guard !weather.list.isEmpty else {
            return 0
        }
        
        let date = Date()
        var count = 0

        for weather in weather.list {
            if weather.dt_txt.toDate()?.day == (date.day + count) {
                count += 1
            }
        }

        return count
    }
}
