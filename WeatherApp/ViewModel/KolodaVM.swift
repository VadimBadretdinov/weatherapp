//
//  KolodaVM.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 24.11.2020.
//

import Foundation
import RxCocoa

final class KolodaVM {
    private let date = Date().dayOfYear
    var weather: [ViewForecastCityWeather]? = nil {
        didSet {
            filterWeather()
        }
    }
    var filteredWeather = BehaviorRelay<[ViewForecastCityWeather]?>(value: nil)

    private func filterWeather() {
        guard let weather = weather else { return }
        var day = date
        var array = [ViewForecastCityWeather]()
        for weatherDay in weather {
            if weatherDay.dt_txt.toDate()?.date.dayOfYear == day {
                array.append(weatherDay)
                day += 1
            }
        }
        filteredWeather.accept(array)
    }
}
