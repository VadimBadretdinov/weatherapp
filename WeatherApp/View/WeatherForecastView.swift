//
//  WeatherForecastView.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 12.11.2020.
//

import UIKit

final class WeatherForecastView: UIView {

    var date: Date? = nil
    var weather: ViewCityWeatherForecast? = nil {
        didSet {
            filterWeather()
            configureView()
        }
    }
    private var filteredWeather: [ViewForecastCityWeather]? = nil
    private let backgroundLayer = CALayer()
    private var weatherLayers: [CALayer] = []
    private var maxCount = 6
    private var blockWidth: CGFloat = 0
    private var markerLayer: CALayer? = nil
    private var markerweatherInfo: CATextLayer? = nil
    private var markerImageLayer: CALayer? = nil
    private let umbrellaLayer = CALayer()
    private var markerPosition: Int = 0
    private let padding: CGFloat = 8

    override func layoutSubviews() {
        super.layoutSubviews()

        blockWidth = self.bounds.width / CGFloat(maxCount)
        backgroundLayer.frame = self.bounds
        for (index, layer) in weatherLayers.enumerated() {
            layer.frame = CGRect(x: (blockWidth * CGFloat(index)), y: 0, width: blockWidth, height: backgroundLayer.bounds.height)
            var yPosition: CGFloat = 0
            if let sublayers = layer.sublayers {
                for sublayer in sublayers {
                    sublayer.frame = CGRect(x: padding, y: yPosition + padding * 2,
                                            width: layer.bounds.width - (padding * 2), height: layer.bounds.height * 0.2)
                    yPosition = sublayer.frame.minY
                }
            }
        }
    }

    func configureView() {
        guard let filteredWeather = filteredWeather else { return }

        let count = (filteredWeather.count > maxCount) ? maxCount : filteredWeather.count
        maxCount = count

        blockWidth = self.bounds.width / CGFloat(maxCount)
        backgroundLayer.frame = self.bounds
        backgroundLayer.cornerRadius = 8
        backgroundLayer.backgroundColor = UIColor.white.cgColor
        self.layer.addSublayer(backgroundLayer)

        for (index, weather) in filteredWeather[0..<maxCount].enumerated() {
            createWeatherBlock(index: index, weather: weather)
        }
        createMarker()
    }

    private func createWeatherBlock(index: Int, weather: ViewForecastCityWeather) {
        let weatherLayer = CALayer()
        weatherLayer.frame = CGRect(x: (blockWidth * CGFloat(index)), y: 0, width: blockWidth, height: backgroundLayer.bounds.height)
        weatherLayer.backgroundColor = UIColor.clear.cgColor
        let weatherInfoTL = CATextLayer()
        weatherInfoTL.frame = CGRect(x: padding, y: padding * 2, width: weatherLayer.bounds.width - (padding * 2), height: weatherLayer.bounds.height * 0.2)
        weatherInfoTL.string = weather.main.temp.toCelsiusString
        weatherInfoTL.fontSize = 12
        weatherInfoTL.alignmentMode = .center
        weatherInfoTL.contentsScale = UIScreen.main.scale
        weatherInfoTL.foregroundColor = UIColor.black.cgColor
        weatherLayer.addSublayer(weatherInfoTL)

        let imageLayer = CALayer()
        let image = TodayWeather.init(rawValue: weather.weather.first?.main ?? "")?.getWeatherIcon()?.cgImage
        imageLayer.frame = CGRect(x: padding, y: weatherInfoTL.frame.height + weatherInfoTL.frame.minY, width: weatherLayer.bounds.width - (padding * 2), height: weatherLayer.bounds.height * 0.5)
        imageLayer.contents = image
        imageLayer.contentsGravity = .resizeAspect
        weatherLayer.addSublayer(imageLayer)
        self.layer.addSublayer(weatherLayer)
        weatherLayers.append(weatherLayer)
    }

    private func createMarker() {
        guard let weather = filteredWeather else { return }

        let layer = CALayer()
        let diff = frame.height * 0.2
        layer.frame = CGRect(x: 0, y: -diff, width: blockWidth, height: frame.height * 1.4)
        layer.backgroundColor = UIColor.red.cgColor
        layer.cornerRadius = 8
        markerLayer = layer

        umbrellaLayer.frame = CGRect(x: padding, y: padding, width: layer.frame.width - (padding * 2), height: diff)
        umbrellaLayer.contents = UIImage(named: "umbrella")?.cgImage
        umbrellaLayer.contentsGravity = .resizeAspect
        umbrellaLayer.isHidden = !(TodayWeather.init(rawValue: weather[markerPosition].weather.first?.main  ?? "") == .Rain)
        layer.addSublayer(umbrellaLayer)

        markerweatherInfo = CATextLayer()
        markerweatherInfo!.frame = CGRect(x: layer.frame.minX + padding, y: diff + (padding * 2),
                                          width: layer.frame.width - (padding * 2), height: frame.height * 0.2)
        markerweatherInfo!.string = weather[markerPosition].main.temp.toCelsiusString
        markerweatherInfo!.fontSize = 12
        markerweatherInfo!.alignmentMode = .center
        markerweatherInfo!.contentsScale = UIScreen.main.scale
        layer.addSublayer(markerweatherInfo!)

        markerImageLayer = CALayer()
        let image = TodayWeather.init(rawValue: weather[markerPosition].weather.first?.main ?? "")?.getWeatherIcon()?.cgImage
        markerImageLayer!.frame = CGRect(x: layer.frame.minX + padding, y: markerweatherInfo!.frame.height + markerweatherInfo!.frame.minY, width: layer.frame.width - 16, height: frame.height * 0.5)
        markerImageLayer!.contents = image
        markerImageLayer!.contentsGravity = .resizeAspect
        layer.addSublayer(markerImageLayer!)

        self.layer.addSublayer(layer)
    }

    private func filterWeather() {
        guard let weather = weather else { return }
        filteredWeather = weather.list.filter({ $0.dt_txt.toDate()?.date.day == date?.day})
    }
}

extension WeatherForecastView {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        updateMarkerByTouch(touches: touches)
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        updateMarkerByTouch(touches: touches)
    }

    private func updateMarkerByTouch(touches: Set<UITouch>) {
        let touch = touches.first
        let location = touch?.location(in: self)
        if let valuePosition = location?.x,
           let weather = filteredWeather, let marker = markerLayer,
                  let imageLayer = markerImageLayer, let info = markerweatherInfo {
            let tempFrame = marker.frame
            let value = Int(valuePosition / blockWidth)

            if weather[markerPosition].weather.first?.main != weather[value].weather.first?.main {
                imageLayer.contents = nil
                imageLayer.contents = TodayWeather.init(rawValue: weather[value].weather.first?.main ?? "")?.getWeatherIcon()?.cgImage
            }
            markerPosition = value
            umbrellaLayer.isHidden = !(TodayWeather.init(rawValue: weather[markerPosition].weather.first?.main  ?? "") == .Rain)
            marker.frame = CGRect(x: blockWidth * CGFloat(value), y: tempFrame.minY, width: tempFrame.width, height: tempFrame.height)
            info.string = weather[markerPosition].main.temp.toCelsiusString
        }
    }
}
