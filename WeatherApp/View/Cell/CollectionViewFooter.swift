//
//  CollectionViewFooter.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 07.11.2020.
//

import Reusable
import UIKit

final class CollectionViewFooter: UICollectionReusableView, Reusable {

    @available(*, unavailable)
    required init(coder _: NSCoder) {
        fatalError("NSCoding not supported")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(pageControl)
        NSLayoutConstraint.activate([
            pageControl.leadingAnchor.constraint(equalTo: leadingAnchor),
            pageControl.trailingAnchor.constraint(equalTo: trailingAnchor),
            pageControl.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    private(set) lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.hidesForSinglePage = true
        pageControl.isUserInteractionEnabled = false
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 0.07500000298, green: 0.0549999997, blue: 0.3179999888, alpha: 1)
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.3650000095, green: 0.3140000105, blue: 0.9959999919, alpha: 1)
        return pageControl
    }()

    private(set) lazy var triangleButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isHidden = true
        return button
    }()
}
