//
//  CurrentWeatherCell.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 07.11.2020.
//

import UIKit
import Reusable

final class CurrentWeatherCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var currentWeather: UILabel!
    @IBOutlet weak var weatherTitle: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.layer.cornerRadius = 8
    }

    func configure(weather: ViewCityWeather) {
        currentWeather.text = weather.main.temp.toCelsiusString
        weatherTitle.text = weather.weather.first?.main ?? ""
        weatherIcon.image = TodayWeather.init(rawValue: weather.weather.first?.main ?? "")?.getWeatherIcon()
    }
}
