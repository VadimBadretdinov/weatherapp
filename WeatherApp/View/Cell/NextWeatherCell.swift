//
//  NextWeatherCell.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 07.11.2020.
//

import UIKit
import Reusable
import SwiftDate

final class NextWeatherCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var currentWeather: UILabel!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.layer.cornerRadius = 8
    }

    func configure(weather: ViewForecastCityWeather) {
        let dayString = weather.dt_txt.toDate()?.weekdayName(.default)
        dayLabel.text = dayString
        currentWeather.text = weather.main.temp.toCelsiusString
        weatherIcon.image = TodayWeather.init(rawValue: weather.weather.first?.main ?? "")?.getWeatherIcon()
        let day = WeatherDay.init(rawValue: dayString?.lowercased() ?? "")
        if let day = day {
            containerView.backgroundColor = day.getColor()
        }
    }
}
