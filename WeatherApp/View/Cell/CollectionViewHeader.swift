//
//  CollectionViewHeader.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 07.11.2020.
//

import UIKit
import Reusable

final class CollectionViewHeader: UICollectionReusableView, Reusable {

    @available(*, unavailable)
    required init(coder _: NSCoder) {
        fatalError("NSCoding not supported")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
        ])
    }

    private(set) lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = UIFont.init(name: "System", size: 15)
        titleLabel.textColor = #colorLiteral(red: 0.07500000298, green: 0.0549999997, blue: 0.3179999888, alpha: 1)
        titleLabel.text = LocCityDetailVC.nextDays.localized
        return titleLabel
    }()
}
