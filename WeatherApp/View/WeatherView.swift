//
//  WeatherView.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 11.11.2020.
//

import UIKit
import Reusable
import SwiftDate

final class WeatherView: UIView, NibOwnerLoadable {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNibContent()
    }

    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      self.loadNibContent()
    }

    func configure(weather: ViewForecastCityWeather) {
        guard let day = WeatherDay.init(rawValue: weather.dt_txt.toDate()?.weekdayName(.default).lowercased() ?? "") else { return }
        infoLabel.text = weather.dt_txt.toDate()?.weekdayName(.default)
        weatherLabel.text = weather.main.temp.toCelsiusString
        imageView.image = TodayWeather.init(rawValue: weather.weather.first?.main ?? "")?.getWeatherIcon()
        self.backgroundColor = day.getColor()
    }
}
