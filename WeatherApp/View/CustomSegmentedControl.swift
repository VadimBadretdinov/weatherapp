//
//  CustomSegmentedControl.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 07.11.2020.
//

import UIKit

typealias SegmentedControlParams = String

final class CustomSegmentedControl: UIView {

    private var buttonTitles:[SegmentedControlParams] = ["Detail","Today","Week"]
    private var buttons: [UIButton]!
    private var selectorView: UIView!
    private let padding: CGFloat = 2

    private let textColor: UIColor = #colorLiteral(red: 0.07450980392, green: 0.05490196078, blue: 0.3176470588, alpha: 1)
    private let selectorViewColor: UIColor = #colorLiteral(red: 0.6784313725, green: 0.6549019608, blue: 0.9960784314, alpha: 1)
    private let selectorTextColor: UIColor = #colorLiteral(red: 0.3647058824, green: 0.3137254902, blue: 0.9960784314, alpha: 1)
    private let cornerRadius: CGFloat = 9

    var buttonTapped: ((Int) -> Void)? {
        didSet {
            buttonTapped?(selectedIndex)
        }
    }

    private(set) var selectedIndex: Int = 0

    override func awakeFromNib() {
        super.awakeFromNib()

        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        self.backgroundColor = .white
    }

    convenience init(frame:CGRect,buttonTitle:[SegmentedControlParams]) {
        self.init(frame: frame)

        self.buttonTitles = buttonTitle
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        updateView()
    }

    @objc func buttonAction(sender:UIButton) {
        for (buttonIndex, btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            if btn == sender {
                let selectorPosition = (frame.width - padding)/CGFloat(buttonTitles.count) * CGFloat(buttonIndex)
                selectedIndex = buttonIndex

                buttonTapped?(buttonIndex)

                UIView.animate(withDuration: 0.3) {
                    self.selectorView.frame.origin.x = self.padding + selectorPosition
                }
                btn.setTitleColor(selectorTextColor, for: .normal)
            }
        }
    }
}

extension CustomSegmentedControl {
    private func updateView() {
        createButton()
        configSelectorView()
        configStackView()
    }

    private func configStackView() {
        let stack = UIStackView(arrangedSubviews: buttons)
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            stack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            stack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            stack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
        ])
    }

    private func configSelectorView() {
        let selectorHeight = frame.height - padding * 2
        let selectorWidth = (frame.width / CGFloat(self.buttonTitles.count)) - padding * 2
        selectorView = UIView(frame: CGRect(x: padding, y: padding, width: selectorWidth, height: selectorHeight))
        selectorView.layer.cornerRadius = cornerRadius
        selectorView.backgroundColor = selectorViewColor
        addSubview(selectorView)
    }

    private func createButton() {
        buttons = [UIButton]()
        buttons.removeAll()
        subviews.forEach({$0.removeFromSuperview()})
        for buttonParams in buttonTitles {
            let button = UIButton(type: .custom)

            button.setTitle(buttonParams, for: .normal)
            button.addTarget(self, action:#selector(CustomSegmentedControl.buttonAction(sender:)), for: .touchUpInside)
            button.setTitleColor(textColor, for: .normal)
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            buttons.append(button)
        }
        if let button = buttons.first {
            button.setTitleColor(selectorTextColor, for: .normal)
        }
    }
}
