//
//  WeatherHelper.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 09.11.2020.
//

import UIKit

enum WeatherDay: String {
    case monday, tuesday, wednesday, thursday, friday, saturday, sunday, current

    func getColor() -> UIColor {
        if self == .current {
            return UIColor(named: "standartTextColor") ?? .gray
        }
        return UIColor(named: "\(self.rawValue)Color") ?? .green
    }
}


enum TodayWeather: String {
    case Rain, Clouds, Clear, Mist, Thunderstorm, Snow

    func getWeatherIcon() -> UIImage? {
        if let image = UIImage(named: "\(self.rawValue)") {
            return image
        }
        return nil
    }
}
