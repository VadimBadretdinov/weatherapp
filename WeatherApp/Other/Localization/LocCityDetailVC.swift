//
//  LocCityDetailVC.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 11.11.2020.
//

import Foundation

enum LocCityDetailVC: String, Localizable {
    case nextDays
}
