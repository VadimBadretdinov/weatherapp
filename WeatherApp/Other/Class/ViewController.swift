//
//  ViewController.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 10.11.2020.
//

import UIKit
import RxSwift

class ViewController: UIViewController {

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let navBar = self as? NavBarConfigure {
            navBar.configureNavigationTitle()
        }

        if let eventSubscriber = self as? EventSubscriber {
            eventSubscriber.subscribeToEvents()
        }
        
    }
}
