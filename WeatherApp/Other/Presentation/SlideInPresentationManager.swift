//
//  SlideInPresentationManager.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 09.11.2020.
//

import UIKit

enum PresentationDirection {
  case left
  case top
  case right
  case bottom
}

struct SlideViewParams {
    let percentOfParentWidth: CGFloat
    let percentOfParentHeight: CGFloat
}

class SlideInPresentationManager: NSObject {

    let direction: PresentationDirection
    let sizeParams: SlideViewParams

    init(direction: PresentationDirection = .bottom, sizeParams: SlideViewParams) {
        self.direction = direction
        self.sizeParams = sizeParams
    }
}

// MARK: - UIViewControllerTransitioningDelegate
extension SlideInPresentationManager: UIViewControllerTransitioningDelegate {
    func presentationController(
      forPresented presented: UIViewController,
      presenting: UIViewController?,
      source: UIViewController
    ) -> UIPresentationController? {
      let presentationController = SlideInPresentationController(
        presentedViewController: presented,
        presenting: presenting,
        direction: direction,
        sizeParams: sizeParams
      )
      return presentationController
    }
}
