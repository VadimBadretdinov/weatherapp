//
//  NetworkManager.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 16.11.2020.
//

import Foundation
import Alamofire

final class NetworkManager {

    func downloadWeather(completion: @escaping (AFResult<NetworkCityWeather>) -> Void) {
        AF.request("https://api.openweathermap.org/data/2.5/weather?lat=\(51)&lon=\(71)&appid=\(Constants.ApiKey)").responseDecodable(of: NetworkCityWeather.self) { response in
            switch response.result {
            case .success(let value):
                completion(.success(value))

            case .failure(let error):
                completion(.failure(error))

            }
        }
    }

    func downloadForecastWeather(completion: @escaping (AFResult<NetworkCityWeatherForecast>) -> Void) {
        AF.request("https://api.openweathermap.org/data/2.5/forecast?lat=\(51)&lon=\(71)&appid=\(Constants.ApiKey)").responseDecodable(of: NetworkCityWeatherForecast.self) { response in
            switch response.result {
            case .success(let value):
                completion(.success(value))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
