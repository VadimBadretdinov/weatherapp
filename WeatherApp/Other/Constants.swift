//
//  Constants.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 07.11.2020.
//

import Foundation

enum Constants {

    static let ApiKey = "6d0a3e9e3cb69799269069ad9131332f"
    static let KelvinToCelsius = 275.15
}
