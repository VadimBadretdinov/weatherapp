//
//  EventSubscriber.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 24.11.2020.
//

import Foundation

protocol EventSubscriber {
    func subscribeToEvents()
}
