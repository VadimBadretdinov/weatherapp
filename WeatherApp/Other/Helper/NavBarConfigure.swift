//
//  NavBarConfigure.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 10.11.2020.
//

import UIKit

protocol NavBarConfigure {
    var navigationTitle: String? { get }

    func configureNavigationTitle()
}

extension NavBarConfigure where Self: ViewController {
    func configureNavigationTitle() {
        
        navigationItem.title = navigationTitle
    }

    var navigationTitle: String? {
        return nil
    }
}
