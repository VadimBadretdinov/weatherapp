//
//  MainCoordinator.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 13.11.2020.
//

import UIKit

protocol Coordinator: class {
  func start()
}

final class MainCoordinator: Coordinator {
    private let navController: UINavigationController
    private let window: UIWindow

    init(navController: UINavigationController, window: UIWindow) {
        self.navController = navController
        self.window = window
    }

    func start() {
        window.rootViewController = navController
        window.makeKeyAndVisible()
        mainVC()
    }

    // MARK: - Navigation
    private func mainVC() {
        let mainVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainVC") as! MainVC
        mainVC.router = self
        navController.pushViewController(mainVC, animated: true)
    }

    private func citiesVC() {
        let citiesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CitiesVC") as! CitiesVC
        navController.pushViewController(citiesVC, animated: true)
    }
}

extension MainCoordinator: MainRouter {
    func toCitiesVC() {
        citiesVC()
    }
}
