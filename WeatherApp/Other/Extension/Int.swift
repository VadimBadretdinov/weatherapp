//
//  String.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 09.11.2020.
//

import Foundation

extension Int {
    var stringValue: String {
        return String(self)
    }
}
