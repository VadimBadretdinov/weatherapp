//
//  Double.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 09.11.2020.
//

import Foundation

extension Double {
    var toCelsiusString: String {
        return (self - Constants.KelvinToCelsius).rounded(.down).toString() + " C"
    }
}
