//
//  DBManager.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 18.11.2020.
//

import UIKit
import CoreData
import SwiftDate

final class DBManager {

    func saveCurrentWeather(weather: ViewCityWeather) {

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        let managedContext = appDelegate.persistentContainer.viewContext



        if let entity = NSEntityDescription.insertNewObject(forEntityName: "CityWeather", into: managedContext) as? CityWeather {
            let coord = Coord(context: managedContext)
            coord.lat = weather.coord.lat
            coord.lon = weather.coord.lon
            entity.coord = coord
            let main = Root(context: managedContext)
            main.temp = weather.main.temp
            entity.main = main
            entity.name = weather.name
            entity.date = Date()

            if let currentWeather = weather.weather.first {
                let entityWeather = Weather(context: managedContext)
                entityWeather.descriptionWeather = currentWeather.description
                entityWeather.main = currentWeather.main
                entity.weather = entityWeather
            }
        }

        do {
            try managedContext.save()
            print("Saved")
        } catch let error as NSError {
            print("Error: \(error)")
        }
    }

    func fetchCurrentWeather() -> ViewCityWeather? {

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }

        let managedContext = appDelegate.persistentContainer.viewContext

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CityWeather")

        do {
            let date = Date()
            if let weather = try managedContext.fetch(fetchRequest) as? [CityWeather],
               date.dayOfYear <= weather.first?.date?.dayOfYear ?? 0 {
                return convertCurrentToViewValue(value: weather)
            }
            return nil
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }

    private func convertCurrentToViewValue(value: [CityWeather]?) -> ViewCityWeather? {
        guard let weatherFirst = value?.first,
              let coord = weatherFirst.coord,
              let main = weatherFirst.main,
              let currentWeather = weatherFirst.weather else {
            print("Error")
            return nil }

        var weatherArray = [ViewWeather]()
        weatherArray.append(ViewWeather(main: currentWeather.main ?? "",
                                            description: currentWeather.descriptionWeather ?? ""))
        return ViewCityWeather(coord: ViewCoord(lon: coord.lon, lat: coord.lat),
                               weather: weatherArray,
                               main: ViewRoot(temp: main.temp),
                               name: weatherFirst.name ?? "")
    }

    func saveForecastWeather(weather: ViewCityWeatherForecast) {

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        let managedContext = appDelegate.persistentContainer.viewContext
        if let entity = NSEntityDescription.insertNewObject(forEntityName: "CityWeatherForecast", into: managedContext) as? CityWeatherForecast {
            let city = City(context: managedContext)
            city.name = weather.city.name
            entity.city = city
            let coord = Coord(context: managedContext)
            coord.lat = weather.city.coord.lat
            coord.lon = weather.city.coord.lon
            entity.city?.coord = coord

            let set = NSMutableOrderedSet()
            for forecast in weather.list {
                let fWeather = ForecastCityWeather(context: managedContext)
                fWeather.dt_txt = forecast.dt_txt
                let main = Root(context: managedContext)
                main.temp = forecast.main.temp
                fWeather.main = main
                if let currentWeather = forecast.weather.first {
                    let weather = Weather(context: managedContext)
                    weather.descriptionWeather = currentWeather.description
                    weather.main = currentWeather.main
                    fWeather.weather = weather
                }
                set.add(fWeather)
            }
            entity.list = set
        }

        do {
            try managedContext.save()
            print("Saved")
        } catch let error as NSError {
            print("Error: \(error)")
        }
    }

    func fetchForecastWeather() -> ViewCityWeatherForecast? {

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }

        let managedContext = appDelegate.persistentContainer.viewContext

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CityWeatherForecast")

        do {
            let date = Date()
            if let weather = try managedContext.fetch(fetchRequest) as? [CityWeatherForecast],
               date.dayOfYear <= weather.first?.date?.dayOfYear ?? 0 {
                return convertForecastToViewValue(value: weather)
            }
            return nil
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }

    private func convertForecastToViewValue(value: [CityWeatherForecast]?) -> ViewCityWeatherForecast? {
        guard let weatherFirst = value?.first,
              let list = weatherFirst.list,
              let city = weatherFirst.city else {
            print("Error")
            return nil }

        var weatherArray = [ViewForecastCityWeather]()

        for case let weather as ForecastCityWeather in list {
            if let main = weather.main, let currentWeather = weather.weather {
                weatherArray.append(ViewForecastCityWeather(main: ViewRoot(temp: main.temp),
                                                            weather: [ViewWeather(main: currentWeather.main ?? "",
                                                                                  description: currentWeather.descriptionWeather ?? "")],
                                                            dt_txt: weather.dt_txt ?? ""))
            }

        }
        return ViewCityWeatherForecast(weather: weatherArray,
                                       city: ViewCity(name: city.name ?? "",
                                                      coord: ViewCoord(lon: city.coord?.lon ?? 51,
                                                                       lat: city.coord?.lat ?? 71)))
    }

}
