//
//  KolodaVC.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 14.11.2020.
//

import UIKit
import Koloda
import SwiftDate
import RxCocoa

final class KolodaVC: ViewController {

    @IBOutlet weak var koloda: KolodaView!
    let viewModel = KolodaVM()

    override func viewDidLoad() {
        super.viewDidLoad()

        koloda.dataSource = self
        koloda.delegate = self
    }
}

extension KolodaVC: KolodaViewDelegate {
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        koloda.resetCurrentCardIndex()
    }
}

extension KolodaVC: KolodaViewDataSource {
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        viewModel.filteredWeather.value?.count ?? 0
    }

    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        guard let filtered = viewModel.filteredWeather.value,
              !filtered.isEmpty else { return UIView() }

        let view = WeatherView()
        view.configure(weather: filtered[index])
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        return view
    }

}

extension KolodaVC: EventSubscriber {
    func subscribeToEvents() {
        viewModel.filteredWeather
            .asObservable()
            .subscribe(onNext: { [weak self] weather in
                guard let _ = weather else { return }
                self?.koloda.reloadData()
            })
            .disposed(by: disposeBag)
    }
}
