//
//  CurrentWeatherVC.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 16.11.2020.
//

import UIKit
import RxCocoa
import RxSwift

final class CurrentWeatherVC: ViewController {

    var viewModel = CurrentWeatherVM()

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var weatherInfo: UILabel!
    @IBOutlet weak var weather: UILabel!

    func configure(currentWeather: ViewCityWeather) {
        backgroundView.layer.cornerRadius = 10
        weatherInfo.text = currentWeather.name
        weather.text = currentWeather.main.temp.toCelsiusString
        weatherIcon.image = TodayWeather.init(rawValue: currentWeather.weather.first?.main ?? "")?.getWeatherIcon()
    }
}

extension CurrentWeatherVC: EventSubscriber {
    func subscribeToEvents() {
        viewModel.weather
            .asObservable()
            .subscribe(onNext: { [weak self] weather in
                guard let currentWeather = weather else { return }
                self?.configure(currentWeather: currentWeather)
            })
            .disposed(by: disposeBag)
    }
}
