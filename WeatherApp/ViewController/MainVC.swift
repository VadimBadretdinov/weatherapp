//
//  MainVC.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 16.11.2020.
//

import UIKit
import CoreData
import RxCocoa

protocol MainRouter: AnyObject {
    func toCitiesVC()
}

final class MainVC: ViewController {
    
    @IBOutlet weak var segmentedControl: CustomSegmentedControl!
    @IBOutlet weak var containerView: UIView!

    private let viewModel = MainVM()
    weak var router: MainRouter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.obtainWeather()

        segmentedControl.buttonTapped = { result in
            switch result {
            case 0:
                self.remove()
                self.add(asChildViewController: self.detail)
            case 1:
                self.remove()
                self.add(asChildViewController: self.currentWeatherDetail)
            case 2:
                self.remove()
                self.add(asChildViewController: self.koloda)
            default:
                break
            }
        }
    }

    private lazy var detail: CityDetailVC = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "CityDetailVC") as! CityDetailVC
        return viewController
    }()

    private lazy var koloda: KolodaVC = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "KolodaVC") as! KolodaVC
        return viewController
    }()

    private lazy var currentWeatherDetail: CurrentWeatherVC = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "CurrentWeatherVC") as! CurrentWeatherVC
        return viewController
    }()

    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)

        containerView.addSubview(viewController.view)

        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        viewController.didMove(toParent: self)
    }

    private func remove() {

        for vc in children {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
    }
    @IBAction func menuBtnAction(_ sender: Any) {
        router?.toCitiesVC()
    }
}

extension MainVC: EventSubscriber {
    func subscribeToEvents() {
        viewModel.currentWeather
            .asObservable()
            .subscribe(onNext: { [weak self] weather in
                guard let currentWeather = weather else { return }
                self?.detail.viewModel.currentWeather.accept(currentWeather)
                self?.currentWeatherDetail.viewModel.weather.accept(currentWeather)
                self?.configureNavigationTitle()
            })
            .disposed(by: disposeBag)

        viewModel.forecastWeather
            .asObservable()
            .subscribe(onNext: { [weak self] weather in
                guard let currentWeather = weather else { return }
                self?.detail.viewModel.forecastWeather.accept(currentWeather)
                self?.koloda.viewModel.weather = currentWeather.list
            })
            .disposed(by: disposeBag)
    }
}

extension MainVC: NavBarConfigure {
    var navigationTitle: String? {
        if let weather = viewModel.currentWeather.value {
            return weather.name
        } else {
            return ""
        }
    }
}
