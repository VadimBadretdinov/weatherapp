//
//  CityDetailVC.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 07.11.2020.
//

import UIKit
import Reusable
import CoreData
import RxCocoa
import RxSwift

final class CityDetailVC: ViewController {

    let viewModel = CityDetailVM()
    private let date = Date()

    enum CityDetailSections: CaseIterable {
        case today, week
    }

    static let headerType = "HeaderType"
    static let footerType = "FooterType"

    var pageControl: UIPageControl?

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(cellType: CurrentWeatherCell.self)
            collectionView.register(cellType: NextWeatherCell.self)
            collectionView.register(supplementaryViewType: CollectionViewHeader.self, ofKind: Self.headerType)
            collectionView.register(supplementaryViewType: CollectionViewFooter.self, ofKind: Self.footerType)

            collectionView.collectionViewLayout = UICollectionViewCompositionalLayout(sectionProvider: { section, _ in
                switch CityDetailSections.allCases[section] {
                case .today:
                    return self.todaySectionLayout()
                case .week:
                    return self.weekSectionLayout()
                }
            })
        }
    }

    private func todaySectionLayout() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.6))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = .init(top: 2, leading: 2, bottom: 2, trailing: 2)
        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: .init(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(0.55)
            ),
            subitem: item,
            count: 1
        )
        group.contentInsets = .init(top: 2, leading: 2, bottom: 2, trailing: 2)
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = .init(top: 8, leading: 16, bottom: 0, trailing: 16)
        return section
    }

    private func weekSectionLayout() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.3))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = .init(top: 2, leading: 2, bottom: 2, trailing: 2)
        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: .init(
                widthDimension: .fractionalWidth(1.0/2.8),
                heightDimension: .fractionalHeight(0.3)
            ),
            subitem: item,
            count: 1
        )
        group.contentInsets = .init(top: 2, leading: 2, bottom: 2, trailing: 2)
        let section = NSCollectionLayoutSection(group: group)
        let header = NSCollectionLayoutBoundarySupplementaryItem(
            layoutSize: .init(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.05)),
            elementKind: Self.headerType,
            alignment: .top
        )
        let footer = NSCollectionLayoutBoundarySupplementaryItem(
            layoutSize: .init(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.05)),
            elementKind: Self.footerType,
            alignment: .bottom
        )
        section.boundarySupplementaryItems = [header, footer]
        section.orthogonalScrollingBehavior = .paging
        section.contentInsets = .init(top: 0, leading: 8, bottom: 0, trailing: 8)
        section.visibleItemsInvalidationHandler = { [weak self] _, point, env in
            self?.pageControl?.currentPage = Int(point.x / env.container.contentSize.width)
        }
        return section
    }
}


extension CityDetailVC: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        CityDetailSections.allCases.count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch CityDetailSections.allCases[section] {
        case .today:
            if let _ = viewModel.currentWeather.value {
                return 1
            }
            return 0
        case .week:
            if let weather = viewModel.forecastWeather.value {
                return viewModel.forecastDaysCount(weather: weather)
            }
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch CityDetailSections.allCases[indexPath.section] {
        case .today:
            let cell = collectionView.dequeueReusableCell(for: indexPath) as CurrentWeatherCell
            if let weather = viewModel.currentWeather.value {
                cell.configure(weather: weather)
            }
            return cell
        case .week:
            let cell = collectionView.dequeueReusableCell(for: indexPath) as NextWeatherCell
            if let weather = viewModel.forecastWeather.value?.list.first(where: { $0.dt_txt.toDate()?.day == (date.day + indexPath.row)}) {
                cell.configure(weather: weather)
            }
            return cell
        }
    }

    func collectionView(_: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case Self.headerType:
            return header(kind: kind, indexPath: indexPath)
        case Self.footerType:
            let footer = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                for: indexPath,
                viewType: CollectionViewFooter.self
            )
            var pageCount = 0
            if let weather = viewModel.forecastWeather.value {
                pageCount = viewModel.forecastDaysCount(weather: weather) / 3
            }
            footer.pageControl.numberOfPages = Int(pageCount)
            pageControl = footer.pageControl
            return footer
        default:
            fatalError("wrong kind \(kind) received")
        }
    }

    private func header(kind: String, indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, for: indexPath, viewType: CollectionViewHeader.self)
        return header
    }

}

extension CityDetailVC: UICollectionViewDelegate {
    func collectionView(_: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch CityDetailSections.allCases[indexPath.section] {
        case .today:
            break
        case .week:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "ForecastWeatherDetailVC") as? ForecastWeatherDetailVC,
               let weather = viewModel.forecastWeather.value {
                vc.loadViewIfNeeded()
                let slideInManager = SlideInPresentationManager(direction: .bottom, sizeParams: SlideViewParams(percentOfParentWidth: 0, percentOfParentHeight: 0.9))
                vc.viewModel.currentWeather.accept(weather.list.first(where: { $0.dt_txt.toDate()?.day == (date.day + indexPath.row)}))
                vc.viewModel.weather.accept(weather)
                vc.transitioningDelegate = slideInManager
                vc.modalPresentationStyle = .custom
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
}

extension CityDetailVC: EventSubscriber {
    func subscribeToEvents() {
        viewModel.currentWeather
            .asObservable()
            .subscribe(onNext: { [weak self] weather in
                guard let _ = weather else { return }
                self?.collectionView.reloadData()
            })
            .disposed(by: disposeBag)

        viewModel.forecastWeather
            .asObservable()
            .subscribe(onNext: { [weak self] weather in
                guard let _ = weather else { return }
                self?.collectionView.reloadData()
            })
            .disposed(by: disposeBag)
    }
}
