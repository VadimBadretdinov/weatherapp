//
//  ForecastWeatherDetailVC.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 09.11.2020.
//

import UIKit
import SwiftDate

final class ForecastWeatherDetailVC: ViewController {
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var infoView: WeatherForecastView!

    let viewModel = ForecastWeatherDetailVM()

    private func configure(weather: ViewForecastCityWeather?) {
        guard let weather = weather,
              let day = WeatherDay.init(rawValue: weather.dt_txt.toDate()?.weekdayName(.default).lowercased() ?? "") else { return }
        let color = day.getColor()
        borderView.layer.cornerRadius = 20
        borderView.backgroundColor = color
        containerView.backgroundColor = color
        tempLabel.text = weather.main.temp.toCelsiusString
        infoLabel.text = weather.weather.first?.main
        imageView.image = TodayWeather.init(rawValue: weather.weather.first?.main ?? "")?.getWeatherIcon()
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

extension ForecastWeatherDetailVC: EventSubscriber {
    func subscribeToEvents() {
        viewModel.weather
            .asObservable()
            .subscribe(onNext: { [weak self] weather in
                guard let _ = weather else { return }
                self?.infoView.weather = weather
            })
            .disposed(by: disposeBag)

        viewModel.currentWeather
            .asObservable()
            .subscribe(onNext: { [weak self] weather in
                guard let weather = weather else { return }
                self?.infoView.date = weather.dt_txt.toDate()?.date
                self?.configure(weather: weather)
            })
            .disposed(by: disposeBag)
    }
}
