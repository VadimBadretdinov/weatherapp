//
//  CitiesVC.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 07.11.2020.
//

import UIKit
import Reusable

final class CitiesVC: ViewController {

    @IBOutlet weak var tableView: UITableView!
    let viewModel = CitiesVM()

}

extension CitiesVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.dataSource.value?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
        if let cities = viewModel.dataSource.value {
            cell.textLabel?.text = cities[indexPath.row].name
        }
        return cell
    }
}

extension CitiesVC: UITableViewDelegate {}

