//
//  NCityWeather.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 07.11.2020.
//

import Foundation

struct NetworkCityWeather: Codable {
    let coord: NetworkCoord
    let weather: [NetworkWeather]
    let main: NetworkRoot
    let name: String

    init(coord: NetworkCoord, weather: [NetworkWeather], main: NetworkRoot, name: String) {
        self.coord = coord
        self.weather = weather
        self.main = main
        self.name = name
    }
}

struct NetworkCoord:Codable {
    let lon: Double
    let lat: Double

    init(lon: Double, lat:Double) {
        self.lon = lon
        self.lat = lat
    }
}

struct NetworkWeather:Codable {
    let main: String
    let description: String
    init(main: String, description: String) {
        self.main = main
        self.description = description
    }
}

struct NetworkRoot: Codable {
    var temp: Double

    init(temp: Double, temp_min: Double, temp_max: Double) {
        self.temp = temp
    }
}
