//
//  NCityWeatherForecast.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 07.11.2020.
//

import Foundation

struct NetworkCityWeatherForecast: Codable {
    let list: [NetworkForecastCityWeather]
    let city: NetworkCity

    init(weather: [NetworkForecastCityWeather], city: NetworkCity) {
        self.list = weather
        self.city = city
    }
}

struct NetworkForecastCityWeather: Codable {
    let main: NetworkRoot
    let weather: [NetworkWeather]
    let dt_txt: String

    init(main: NetworkRoot, weather: [NetworkWeather], dt_txt: String) {
        self.main = main
        self.weather = weather
        self.dt_txt = dt_txt
    }
}

struct NetworkCity: Codable {
    let name: String
    let coord: NetworkCoord

    init(name: String, coord: NetworkCoord) {
        self.name = name
        self.coord = coord
    }
}
