//
//  VCityWeatherForecast.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 18.11.2020.
//

import Foundation

struct ViewCityWeatherForecast {
    let list: [ViewForecastCityWeather]
    let city: ViewCity

    init(weather: NetworkCityWeatherForecast) {
        self.list = weather.list.map { ViewForecastCityWeather(weather: $0)}
        self.city = ViewCity(networkCity: weather.city)
    }

    init(weather: [ViewForecastCityWeather], city: ViewCity) {
        self.list = weather
        self.city = city
    }
}

struct ViewForecastCityWeather {
    let main: ViewRoot
    let weather: [ViewWeather]
    let dt_txt: String

    init(weather: NetworkForecastCityWeather) {
        self.main = ViewRoot(networkRoot: weather.main)
        self.weather = weather.weather.map { ViewWeather(networkWeather: $0)}
        self.dt_txt = weather.dt_txt
    }

    init(main: ViewRoot, weather: [ViewWeather], dt_txt: String) {
        self.main = main
        self.weather = weather
        self.dt_txt = dt_txt
    }
}

struct ViewCity {
    let name: String
    let coord: ViewCoord

    init(networkCity: NetworkCity) {
        self.name = networkCity.name
        self.coord = ViewCoord(networkCoord: networkCity.coord)
    }

    init(name: String, coord: ViewCoord) {
        self.name = name
        self.coord = coord
    }
}
