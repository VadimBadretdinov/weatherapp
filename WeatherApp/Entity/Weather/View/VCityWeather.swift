//
//  VCityWeather.swift
//  WeatherApp
//
//  Created by Vadim Badretdinov on 18.11.2020.
//

import Foundation

struct ViewCityWeather {
    let coord: ViewCoord
    let weather: [ViewWeather]
    let main: ViewRoot
    let name: String

    init(networkWeather: NetworkCityWeather) {
        self.coord = ViewCoord(networkCoord: networkWeather.coord)
        self.weather = networkWeather.weather.map { ViewWeather(networkWeather: $0) }
        self.main = ViewRoot(networkRoot: networkWeather.main)
        self.name = networkWeather.name
    }

    init(coord: ViewCoord, weather: [ViewWeather], main: ViewRoot, name: String) {
        self.coord = coord
        self.weather = weather
        self.main = main
        self.name = name
    }

}

struct ViewCoord {
    let lon: Double
    let lat: Double

    init(networkCoord: NetworkCoord) {
        self.lon = networkCoord.lon
        self.lat = networkCoord.lat
    }

    init(lon: Double, lat:Double) {
        self.lon = lon
        self.lat = lat
    }
}

struct ViewWeather {
    let main: String
    let description: String

    init(networkWeather: NetworkWeather) {
        self.main = networkWeather.main
        self.description = networkWeather.description
    }

    init(main: String, description: String) {
        self.main = main
        self.description = description
    }
}

struct ViewRoot {
    var temp: Double

    init(networkRoot: NetworkRoot) {
        self.temp = networkRoot.temp
    }

    init(temp: Double) {
        self.temp = temp
    }
}
