//
//  CityWeatherForecast+CoreDataProperties.swift
//  
//
//  Created by Vadim Badretdinov on 17.11.2020.
//
//

import Foundation
import CoreData


extension CityWeatherForecast {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CityWeatherForecast> {
        return NSFetchRequest<CityWeatherForecast>(entityName: "CityWeatherForecast")
    }

    @NSManaged public var city: Root?
    @NSManaged public var list: NSSet?

}

// MARK: Generated accessors for list
extension CityWeatherForecast {

    @objc(addListObject:)
    @NSManaged public func addToList(_ value: CityWeatherForecast)

    @objc(removeListObject:)
    @NSManaged public func removeFromList(_ value: CityWeatherForecast)

    @objc(addList:)
    @NSManaged public func addToList(_ values: NSSet)

    @objc(removeList:)
    @NSManaged public func removeFromList(_ values: NSSet)

}
