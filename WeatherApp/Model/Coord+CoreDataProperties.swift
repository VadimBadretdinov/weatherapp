//
//  Coord+CoreDataProperties.swift
//  
//
//  Created by Vadim Badretdinov on 17.11.2020.
//
//

import Foundation
import CoreData


extension Coord {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Coord> {
        return NSFetchRequest<Coord>(entityName: "Coord")
    }

    @NSManaged public var lat: Double
    @NSManaged public var lon: Double

}
