//
//  CityWeather+CoreDataProperties.swift
//  
//
//  Created by Vadim Badretdinov on 17.11.2020.
//
//

import Foundation
import CoreData


extension CityWeather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CityWeather> {
        return NSFetchRequest<CityWeather>(entityName: "CityWeather")
    }

    @NSManaged public var name: String?
    @NSManaged public var coord: Coord?
    @NSManaged public var main: Root?
    @NSManaged public var weather: NSSet?

}

// MARK: Generated accessors for weather
extension CityWeather {

    @objc(addWeatherObject:)
    @NSManaged public func addToWeather(_ value: Weather)

    @objc(removeWeatherObject:)
    @NSManaged public func removeFromWeather(_ value: Weather)

    @objc(addWeather:)
    @NSManaged public func addToWeather(_ values: NSSet)

    @objc(removeWeather:)
    @NSManaged public func removeFromWeather(_ values: NSSet)

}
