//
//  CityWeather+CoreDataProperties.swift
//  
//
//  Created by Vadim Badretdinov on 23.11.2020.
//
//

import Foundation
import CoreData


extension CityWeather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CityWeather> {
        return NSFetchRequest<CityWeather>(entityName: "CityWeather")
    }

    @NSManaged public var name: String?
    @NSManaged public var coord: Coord?
    @NSManaged public var main: Root?
    @NSManaged public var weather: NSOrderedSet?

}

// MARK: Generated accessors for weather
extension CityWeather {

    @objc(insertObject:inWeatherAtIndex:)
    @NSManaged public func insertIntoWeather(_ value: Weather, at idx: Int)

    @objc(removeObjectFromWeatherAtIndex:)
    @NSManaged public func removeFromWeather(at idx: Int)

    @objc(insertWeather:atIndexes:)
    @NSManaged public func insertIntoWeather(_ values: [Weather], at indexes: NSIndexSet)

    @objc(removeWeatherAtIndexes:)
    @NSManaged public func removeFromWeather(at indexes: NSIndexSet)

    @objc(replaceObjectInWeatherAtIndex:withObject:)
    @NSManaged public func replaceWeather(at idx: Int, with value: Weather)

    @objc(replaceWeatherAtIndexes:withWeather:)
    @NSManaged public func replaceWeather(at indexes: NSIndexSet, with values: [Weather])

    @objc(addWeatherObject:)
    @NSManaged public func addToWeather(_ value: Weather)

    @objc(removeWeatherObject:)
    @NSManaged public func removeFromWeather(_ value: Weather)

    @objc(addWeather:)
    @NSManaged public func addToWeather(_ values: NSOrderedSet)

    @objc(removeWeather:)
    @NSManaged public func removeFromWeather(_ values: NSOrderedSet)

}
