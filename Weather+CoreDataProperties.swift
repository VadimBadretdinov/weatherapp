//
//  Weather+CoreDataProperties.swift
//  
//
//  Created by Vadim Badretdinov on 23.11.2020.
//
//

import Foundation
import CoreData


extension Weather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Weather> {
        return NSFetchRequest<Weather>(entityName: "Weather")
    }

    @NSManaged public var descriptionWeather: String?
    @NSManaged public var main: String?

}
